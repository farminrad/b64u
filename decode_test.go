package b64u

import (
	"bytes"
	"errors"
	"testing"
)

var url = []byte("https://example.com")

func TestFindHttpUrl_invalidInputs(t *testing.T) {
	set := make([][]byte, 0)
	set = append(set, []byte("https/examlple/com"))
	set = append(set, []byte("example.com"))
	for _, b := range set {
		if u := findHttpUrl(b); u != nil {
			t.Fatalf("input : %q; expected nil but got %q", b, u)
		}

	}
}

func TestFindHttpUrl(t *testing.T) {
	// valid inputs
	set := make([][]byte, 0)
	b := []byte("\x00\xachttps://example.com\x10")
	set = append(set, b)
	b = url
	set = append(set, b)
	for _, b := range set {
		if u := findHttpUrl(b); !bytes.Equal(url, u) {
			t.Fatalf("input : %q; expected %q but got %q", b, url, u)
		}
		if findHttpUrl(url) == nil {
			t.Fatalf("input : %q; expected %q but got nil", url, url)
		}
	}
}

func TestDecode_InvalidInputs(t *testing.T) {
	// nil case
	_, _, err := Decode(nil)
	testInvalidInput(t, nil, err)
	//empty byte array
	in := []byte{}
	_, _, err = Decode(in)
	testInvalidInput(t, in, err)
	//byte array not containing base64 encoded data
	in = []byte("hello$%")
	_, _, err = Decode(in)
	testInvalidInput(t, in, err)
}

func testInvalidInput(t *testing.T, in any, err error) {
	if err == nil {
		t.Fatalf("input : %q; expected error, got nil", in)
	}
}

func TestDecode_ValidInputs_containsUrl(t *testing.T) {
	s := "\x00https://example.com\x00\xac"
	test, isUrl, err := Decode([]byte(s))
	if err != nil {
		t.Fatalf("intpu : %q; no error expected but got %T : %v", s, err, err)
	}
	if !isUrl {
		t.Fatalf("intpu : %q; expected isUrl = true, but get false", s)
	}
	if !bytes.Equal(test, url) {
		t.Errorf("input : %v, expected %q, got %q", s, url, test)
	}
}

// Input data is a valide base64 encoded string.
func TestDecode_ValidInputs_Base64EncodedString(t *testing.T) {
	// map containing test sets
	set := make(map[string][]byte)
	// invalid padding
	set["aHR0cHM6Ly9leGFtcGxlLmNvbQ="] = url
	// aHR0cHM6Ly9l([bad)})eGFtcGxlLmNvbQ==
	set["YUhSMGNITTZMeTlsKFtiYWQpfSllR0Z0Y0d4bExtTnZiUT09"] = []byte("aHR0cHM6Ly9leGFtcGxlLmNvbQ==")
	for k, v := range set {
		test, isUrl, err := Decode([]byte(k))
		if err != nil {
			t.Fatalf("input : %v; no error expected but got %T : %v", k, err, err)
		}
		if isUrl {
			t.Fatalf("intput : %v; expected isUrl = false, but got true", k)
		}
		if !bytes.Equal(test, v) {
			t.Errorf("input : %v, expected %q, got %q", k, v, test)
		}
	}
}

func TestDecodeN_Valide_NoUrlFound(t *testing.T) {
	s := `abcd`
	val, err := DecodeN([]byte(s), 1)
	if !errors.Is(err, ErrNoUrlFound) {
		t.Fatalf("ErrNoUrlFound expected, but got %T : %v", err, err)
	}
	if val != nil {
		t.Fatalf("nil expected, but got a %T in %v", val, val)
	}
}

func TestDecodeN_Valide_ContainsUrl(t *testing.T) {
	s := `WVVoU01HTklUVFpNZVRsc1pVe3tBXV1kR1cxdDdRa0J7YmFkfV1da2ZYMWRYVjEwWTBkNGJFeHRUblppVVQwOQ==`
	val, err := DecodeN([]byte(s), 10)
	if err != nil {
		t.Fatalf("input : %q; no error expected but got %T : %v", s, err, err)
	}
	if !bytes.Equal(url, val) {
		t.Fatalf("expected %q but got %q", url, val)
	}
}

func TestDecodeN_InvalidInput(t *testing.T) {
	s := "%$@"
	_, err := DecodeN([]byte(s), 2)
	if err == nil {
		t.Fatalf("input : %q; error expected but got nil", s)
	}
}
