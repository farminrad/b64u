package b64u

import (
	"encoding/base64"
	"errors"
	"fmt"
	"regexp"
)

// Docode decodes an obfuscated base 64 encoded url.
// It returns the first url found inside b and true if it was found;
// otherwise it removes all parasitic characters and returns the decoded byte array, and false.
// It returns an error when b is an invalide base 64 string.
// The input data cannot be nil or []byte{}
func Decode(b []byte) ([]byte, bool, error) {
	if len(b) == 0 {
		return nil, false, errors.New("empty or nil input")
	}
	if b := findHttpUrl(b); b != nil {
		return b, true, nil
	}
	b = removePadding(b)
	res := make([]byte, base64.RawURLEncoding.DecodedLen(len(b)))
	_, err := base64.RawURLEncoding.Decode(res, b)
	if err != nil {
		return nil, false, fmt.Errorf("cannot decode %q : %v", b, err.Error())
	}
	res = clean(res)
	return res, false, nil
}

// DocodeN applies n times Decode on the b byte array until finding a url.
// It returns the url, if found, as a byte array or an error.
// The error is NoUrlFoundError only if no url is found after n attemps.
func DecodeN(b []byte, n uint) ([]byte, error) {
	var (
		res = b
		ok  = false // true if a url is found
		err error
	)
	for i := uint(0); i < n; i++ {
		res, ok, err = Decode(res)
		if err != nil {
			return nil, err
		}
		// a url is found
		if ok {
			return res, nil
		}
	}
	return nil, fmt.Errorf("%w : %q", ErrNoUrlFound, b)
}

// ErrNoUrlFound indicates no url was found in input data
var ErrNoUrlFound = errors.New("no url found")

// obfuscation string pattern
const obfPtrn = `([\[\{\(\'\"|\"\'\)\}\]\~\#\*]+)([^\[\{\(\'\"\)\}\]\~\#\*]+)([\[\{\(\'\"|\"\'\)\}\]\~\#\*]+)`

// obfuscation regex
var obfRgx, _ = regexp.Compile(obfPtrn)

// clean remove all obfuscating injections form the string s
func clean(b []byte) []byte {
	return obfRgx.ReplaceAll(b, []byte{})
}

// http(s) pattern
const httpPtrn = `(?:http(s)?:\/\/)+[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+`

// http(s) regex
var httpRgx, _ = regexp.Compile(httpPtrn)

// findHttpUrl return the first http url found in the byte array.
// It return value of nil indicates no http url found.
func findHttpUrl(b []byte) []byte {
	return httpRgx.Find(b)
}

// base64 padding pattern
const padPtrn = `==?`

// base64 padding regex
var padRgx, _ = regexp.Compile(padPtrn)

// removePadding returns a byte array with no base64 padding characters
func removePadding(b []byte) []byte {
	return padRgx.ReplaceAll(b, []byte{})
}
